from django.conf.urls import patterns, include, url
from django.contrib import admin
from django_markdown import flatpages
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from pyblog import settings

admin.autodiscover()
flatpages.register()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url('', include('news.urls')),
    url('', include('account.urls')),
    url('^markdown/', include( 'django_markdown.urls')),
    url(r'', include('social_auth.urls')),
    url(r'^search/', include('haystack.urls'), name='search'),
    url(r'^comments/', include('django_comments.urls'), name='url'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)