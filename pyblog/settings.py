"""
Django settings for pyblog project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
import random

SITE_ID = 1

VK_APP_ID = '4427846'
VKONTAKTE_APP_ID = VK_APP_ID
VK_API_SECRET = 'BFJNtTLBX5VPcKk2I4Hv'
VKONTAKTE_APP_SECRET = VK_API_SECRET

GOOGLE_OAUTH2_CLIENT_ID = '58944164036-vk3nvr4mcjtqvijkh8vgrls0i3edqnfs.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'ZTzqc3D7o1A0HZfuhNY2steu'

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.contrib.vk.VKOpenAPIBackend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_DEFAULT_USERNAME = lambda: random.choice(['Darth_Vader', 'Obi-Wan_Kenobi', 'R2-D2', 'C-3PO', 'Yoda'])
SOCIAL_AUTH_CREATE_USERS = True
SOCIAL_AUTH_USER_MODEL = 'account.User'
CUSTOM_USER_MODEL = 'account.User'
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/profile/'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
    'social_auth.context_processors.social_auth_by_name_backends',
)

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details'
)

ENDLESS_PAGINATION_PER_PAGE = 5

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_DIRS = [
    os.path.join(BASE_DIR,  'templates/'),
]

MEDIA_ROOT = os.path.join(BASE_DIR,  'uploads')
MEDIA_URL = '/uploads/'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k0jpt_)qwlk##9&i3@wd197jlg+bqih+gs9$ivr=hly=j7u96u'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

SOUTH_MIGRATION_MODULES = {
        'taggit': 'taggit.south_migrations',
    }

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    #'django.contrib.comments'
    'south',
    'news',
    'taggit',
    'account',
    'endless_pagination',
    'django_markdown',
    'markdown_deux',
    'social_auth',
    'sorl.thumbnail',
    'haystack',
    'django_comments'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'pyblog.urls'

WSGI_APPLICATION = 'pyblog.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pyblog',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

#redis

CACHES = {
    "default": {
        "BACKEND": "redis_cache.cache.RedisCache",
        "LOCATION": "unix:/var/run/redis/redis.sock:1",
        "OPTIONS": {
            "PICKLE_VERSION": -1,
            "CLIENT_CLASS": "redis_cache.client.DefaultClient",
        },
    },
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'default'


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

STATIC_URL = '/static/'

ROOT_PATH = os.path.join(os.path.dirname(__file__), '..')  # up one level from settings.py
STATICFILES_DIRS = (
    os.path.abspath(os.path.join(ROOT_PATH, 'static')),     # static is on root level
)

AUTH_USER_MODEL = 'account.User'  # WARNING!!! It is enable new scheme of auth May be you have to create new superuser!

LOGIN_URL = '/login/'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr'
    },
}
