# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView
from django.core.cache import cache
from endless_pagination.views import AjaxListView
from news.models import News, Categories, Images
import datetime as dt
from operator import itemgetter
import choices
from django.shortcuts import render_to_response


class NewsList(AjaxListView):
    context_object_name = 'news_list'
    queryset = News.objects.filter(status=choices.ACTIVE) \
        .filter(pub_date__lt=dt.datetime.now()).order_by('-pub_date')
    template_name = 'news/category.html'
    page_template = 'news/list.html'

    def get_context_data(self, **kwargs):
        context = super(NewsList, self).get_context_data(**kwargs)
        context['categories_list'] = Categories.objects.filter(activity=True).order_by('sort')
        return context


class IndexView(NewsList):
    template_name = 'news/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        period = dt.datetime.now()
        context['tiles_list'] = sort_by_view(News.objects.only('id', 'main_image', 'title', 'slug'))[:7]
        top_categories = Categories.objects.order_by('sort')[:4]
        context['top_list'] = {}
        period = dt.datetime.now()
        for t in top_categories:
            context['top_list'][t] = sort_by_view(t.news_set.exclude(id__in=[x.id for x in context['tiles_list']])
                                                    .only('id', 'title', 'slug'))[:5]
        return context


class NewsFromCategory(NewsList):
    def get_queryset(self):
        self.current_category = Categories.objects.get(pk=self.kwargs['category'])
        return self.current_category.news_set.filter(status=choices.ACTIVE) \
            .filter(pub_date__lt=dt.datetime.now()).order_by('-pub_date')

    def get_context_data(self, **kwargs):
        context = super(NewsFromCategory, self).get_context_data(**kwargs)
        context['current_category'] = self.current_category.id
        context['content_title'] = self.current_category.title
        return context


class NewsImportant(NewsList):
    def get_queryset(self):
        queryset = super(NewsImportant, self).get_queryset()
        return queryset.filter(important=True)

    def get_context_data(self, **kwargs):
        context = super(NewsImportant, self).get_context_data(**kwargs)
        context['content_title'] = 'Important'
        return context


class NewsTop(NewsList):
    def get_queryset(self):
        queryset = super(NewsTop, self).get_queryset()
        ids = get_view_statistic()
        qs = sort_by_view(queryset.filter(id__in=[x[0] for x in ids]))
        return qs

    def get_context_data(self, **kwargs):
        context = super(NewsTop, self).get_context_data(**kwargs)
        context['content_title'] = 'Top News'
        return context


class TagList(NewsList):
    def get_queryset(self):
        queryset = super(TagList, self).get_queryset()
        qs = queryset.filter(tags__slug=self.kwargs.get('slug'))
        return qs

    def get_context_data(self, **kwargs):
        context = super(TagList, self).get_context_data(**kwargs)
        context['content_title'] = 'News with tag '+self.kwargs.get('slug').replace('-', ' ')
        return context


class NewsDetail(DetailView):
    template_name = 'news/detail.html'

    def get_object(self):
        return News.objects.get(slug=self.kwargs.get('slug'))

    def get_context_data(self, **kwargs):
        context = super(NewsDetail, self).get_context_data(**kwargs)
        context['top_counter'] = self.add_top_counter(self.get_object().pk)
        return context

    def add_top_counter(self, id):
        count = cache.get('top_counter_'+str(id))
        cache.set('top_counter_'+str(id), int(count)+1, timeout=0)
        return count


def get_view_statistic():
    counters = []
    for x in cache.keys('top_counter_*'):
        try:
            counters.append([int(x[12:]), int(cache.get(x))])
        except ValueError:
            pass
    return sorted(counters, key=itemgetter(1), reverse=True)


def sort_by_view(qs):
    ids = get_view_statistic()
    qs_ids = [x.pk for x in qs]
    qs = list(qs)
    print(qs)
    print(ids)
    qs.sort(key=lambda t: [x[0] for x in ids if x[0] in qs_ids].index(t.pk))
    return qs

