# -*- coding: utf-8 -*-
from django.db import models
from taggit.managers import TaggableManager
import datetime
import choices
from sorl.thumbnail import get_thumbnail
from django.core.files.base import ContentFile

#это для последующей странсляции с других сайтов
#from django.contrib.syndication.feeds import Feed


class Categories(models.Model):
    title = models.CharField(max_length=50)
    meta_tags = TaggableManager()
    sort = models.PositiveSmallIntegerField()
    activity = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.title


class News(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique_for_date='pub_date')
    annotation = models.CharField(max_length=500)
    body = models.TextField()
    mask = models.PositiveSmallIntegerField(
        default=choices.ALL,
        choices=choices.MASK
    )
    main_image = models.ImageField(upload_to="main")
    pub_date = models.DateTimeField('Date published')
    create_date = models.DateTimeField(auto_now_add=True, default=datetime.datetime.now())
    status = models.PositiveSmallIntegerField(
        default=choices.ACTIVE,
        choices=choices.STATUS
    )
    important = models.BooleanField(default=False)
    tags = TaggableManager()
    #todo
    region = models.CharField(blank=True, max_length=100)
    category = models.ForeignKey(Categories)
    enable_comments = models.BooleanField(default=True)

    class Meta:
        ordering = ('-pub_date',)
        get_latest_by = 'pub_date'
        verbose_name_plural = 'News'

    def save(self, *args, **kwargs):
        super(News, self).save(*args, **kwargs)
        resized = unicode(get_thumbnail(self.main_image, "150x150").url)
        self.main_image = resized
        super(News, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/news/%s" % self.slug

    def __unicode__(self):
        return self.title


class Images(models.Model):
    #todo
    path = models.ImageField(upload_to='images', height_field=300, width_field=600)
    new = models.ForeignKey(News)

