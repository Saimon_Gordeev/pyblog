from django.contrib import admin
from django.core.cache import cache
from django import forms
from django_markdown.admin import MarkdownModelAdmin
from news.models import Categories, News, Images


class NewsAdminForm(forms.ModelForm):
    annotation = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = News


class NewsAdmin(MarkdownModelAdmin):
    form = NewsAdminForm
    list_display = ('category', 'title', 'pub_date', 'enable_comments', 'status',
                    'mask', 'annotation', 'main_image', 'important')
    search_fields = ['title']
    list_filter = ('pub_date', 'enable_comments', 'status', 'mask')
    prepopulated_fields = {"slug": ('title',)}
    fieldsets = [(None, {'fields': (('category', 'title', 'status', 'mask'), 'annotation', 'body',
                                    ('important', 'enable_comments'), 'tags', 'slug', 'main_image', 'pub_date')})]

    def save_model(self, request, obj, form, change):
        obj.save()
        if not cache.get('top_counter_'+str(obj.id)):
            cache.set('top_counter_'+str(obj.id), 1, timeout=0)


admin.site.register(Categories)
admin.site.register(News, NewsAdmin)
