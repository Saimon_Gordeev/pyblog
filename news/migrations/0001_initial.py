# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Categories'
        db.create_table(u'news_categories', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sort', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('activity', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'news', ['Categories'])

        # Adding model 'News'
        db.create_table(u'news_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('annotation', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('mask', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('main_image', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('view_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('top', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['news.Categories'])),
        ))
        db.send_create_signal(u'news', ['News'])

        # Adding model 'Images'
        db.create_table(u'news_images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('weight', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('new', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['news.News'])),
        ))
        db.send_create_signal(u'news', ['Images'])


    def backwards(self, orm):
        # Deleting model 'Categories'
        db.delete_table(u'news_categories')

        # Deleting model 'News'
        db.delete_table(u'news_news')

        # Deleting model 'Images'
        db.delete_table(u'news_images')


    models = {
        u'news.categories': {
            'Meta': {'object_name': 'Categories'},
            'activity': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sort': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'news.images': {
            'Meta': {'object_name': 'Images'},
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['news.News']"}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'weight': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'news.news': {
            'Meta': {'object_name': 'News'},
            'annotation': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['news.Categories']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_image': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mask': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'top': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'view_date': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['news']