# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Images.weight'
        db.delete_column(u'news_images', 'weight')

        # Deleting field 'Images.height'
        db.delete_column(u'news_images', 'height')


        # Changing field 'Images.path'
        db.alter_column(u'news_images', 'path', self.gf('django.db.models.fields.files.ImageField')(max_length=100))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Images.weight'
        raise RuntimeError("Cannot reverse this migration. 'Images.weight' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Images.weight'
        db.add_column(u'news_images', 'weight',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Images.height'
        raise RuntimeError("Cannot reverse this migration. 'Images.height' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Images.height'
        db.add_column(u'news_images', 'height',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(),
                      keep_default=False)


        # Changing field 'Images.path'
        db.alter_column(u'news_images', 'path', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'news.categories': {
            'Meta': {'object_name': 'Categories'},
            'activity': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sort': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'news.images': {
            'Meta': {'object_name': 'Images'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['news.News']"}),
            'path': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'news.news': {
            'Meta': {'ordering': "('-pub_date',)", 'object_name': 'News'},
            'annotation': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['news.Categories']"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 5, 30, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'enable_comments': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'mask': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'top': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['news']