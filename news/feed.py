from django.contrib.syndication.views import Feed
from news.models import News


class LatestEntriesFeed(Feed):
    title = "Pyblogitechart site news"
    link = "/sitenews/"
    description = "Updates on changes and additions to pyblogitechart."

    def items(self):
        return News.objects.order_by('-pub_date')[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.annotation

    def item_pubdate(self, item):
        return item.pub_date