ALL, TITLE, ANNOTATION, HIDDEN = range(0, 4)
MASK = ((ALL, 'all'),
        (TITLE, 'title'),
        (ANNOTATION, 'annotation'),
        (HIDDEN, 'hidden'))

ACTIVE, INACTIVE, ARCHIVE = range(0, 3)
STATUS = (  (ACTIVE, 'active'),
            (INACTIVE, 'inactive'),
            (ARCHIVE, 'archive'))

