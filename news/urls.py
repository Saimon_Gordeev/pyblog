# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from news.views import IndexView, NewsDetail, NewsFromCategory, NewsImportant, NewsTop, TagList
from django.conf.urls.static import static
from django.conf import settings
from news.feed import LatestEntriesFeed

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='news_index'),
    url(r'^important$', NewsImportant.as_view(), name='news_important'),
    url(r'^top$', NewsTop.as_view(), name='news_top'),
    url(r'^news/(?P<slug>[a-zA-Z0-9_.-]+)$', NewsDetail.as_view(), name='news_read'),
    url(r'^categories/(?P<category>[0-9]+)$', NewsFromCategory.as_view(), name='categories'),
    url(r'^feed/$', LatestEntriesFeed(), name='feed'),
    url(r'^tag/(?P<slug>[-\w]+)$', TagList.as_view(), name='search_tag'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
