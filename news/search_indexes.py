import datetime
from haystack import indexes
from news.models import News


class NewsIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    #author = indexes.CharField(model_attr='user')
    pub_date = indexes.DateTimeField(model_attr='pub_date', faceted=True)
    annotation = indexes.CharField(model_attr='annotation', faceted=True)

    def get_model(self):
        return News

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        #return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())
        return self.get_model().objects.all()

