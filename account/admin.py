from django.contrib import admin

# Register your models here.

from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from account.models import User
from account.forms import UserChangeForm, RegistrationForm
from sorl.thumbnail.shortcuts import get_thumbnail


class GroupAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('permissions',)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.rel.to.objects)
            kwargs['queryset'] = qs.select_related('content_type')
        return super(GroupAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)


class UserAdmin(UserAdmin):
    form = UserChangeForm

    add_form = RegistrationForm

    add_fieldsets = (
        (None, {'classes': ('wide',),
                'fields': ('username', 'email', 'password1', 'password2',)}),
    )
    list_display = ('email', 'username', 'is_admin', 'is_active', 'get_thumbnail_html',)
    list_filter = ('is_active', 'is_superuser',)
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password')}),
        ('Personal info', {'fields': ('date_of_birth', 'first_name', 'last_name', 'telephone', 'job', 'avatar',)}),
        ('Permissions', {'fields': ('is_active', 'is_superuser', 'is_admin',
                                    'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login',)}),
    )
    filter_horizontal = ('user_permissions',)
    search_fields = ('email',)
    ordering = ('email',)

admin.site.register(User, UserAdmin)
#admin.site.unregister(Group)  # for first. Will write staff for groups tomorrow
