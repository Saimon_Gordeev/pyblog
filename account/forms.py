from django import forms
from account.models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField
import re


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User

    def clean_password(self):
        return self.initial["password"]


class RegistrationForm(forms.ModelForm):

    username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label="Username", error_messages={'invalid': "This value must contain only letters, numbers and underscores." })
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label="Email address")
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label="Password (again)")

    class Meta:
        model = User
        fields = ('email', 'username')

    def clean_username(self):
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError("The username already exists. Please try another one.")

    def clean_email(self):
        try:
            user = User.objects.get(email__iexact=self.cleaned_data['email'])
        except User.DoesNotExist:
            return self.cleaned_data['email']
        raise forms.ValidationError("The email already exists. Please try another one.")

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError("The two password fields did not match.")
        return self.cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


'''class SignInForm(forms.Form):
    username = forms.EmailField(widget=forms.TextInput(attrs={'humanReadable': 'E-mail'}), label='E-mail')
    password = forms.CharField(min_length=6, max_length=32, widget=forms.PasswordInput(attrs={'humanReadable': 'Password'}), label='Password')

    def clean(self):
        if not self._errors:
            cleaned_data = super(SignInForm, self).clean()
            username = cleaned_data.get('username')
            password = cleaned_data.get('password')
            try:
                user = User.objects.get(username=username)
                if not user.check_password(password):
                    raise forms.ValidationError('Not correct email or password')
                elif not user.is_active:
                    raise forms.ValidationError('User have been blocked')
            except User.DoesNotExist:
                raise forms.ValidationError('User does not exists')
            return cleaned_data'''