from account.forms import *
from account.models import User
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
import json


@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            password=form.cleaned_data['password1'],
                                            email=form.cleaned_data['email']
                                            )
            return render_to_response('account/success.html', {'user': user})
        else:
            if request.is_ajax():
                # Prepare JSON for parsing
                errors_dict = {}
                if form.errors:
                    for error in form.errors:
                        e = form.errors[error]
                        errors_dict[error] = unicode(e)

                return HttpResponseBadRequest(json.dumps(errors_dict))
            else:
                # render() form with errors (No AJAX)
                pass
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {'form': form})

    return render_to_response('account/register.html', variables,)


def user_login(request):
    context = RequestContext(request)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return render_to_response('account/profile.html',  {'user': user}, context)
            else:
                return HttpResponse("Account is disabled.")
        else:
            return HttpResponse("Invalid login details supplied.")
    else:
        return render_to_response('base.html', context)


@login_required  # only those logged in can access the view
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def profile(request):
    context = RequestContext(request)
    u = User.objects.get(pk=request.user.pk)

    if request.method == "POST":
        first_name = request.POST['first-name']  # because of double response to server =(
        last_name = request.POST['last-name']
        telephone = request.POST['telephone']
        job = request.POST['job']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        u.first_name = first_name if first_name else u.first_name
        u.last_name = last_name if last_name else u.last_name
        u.telephone = telephone if telephone else u.telephone
        u.job = job if job else u.job
        if password2 and password1 and password2 == password1:
            u.set_password(password1)
        u.save()
        u = User.objects.get(pk=request.user.pk)
        return render_to_response('account/profile.html',  {'proc': check(u), 'user': u}, context)
    else:
        return render_to_response('account/profile.html', {'proc': check(u), 'user': u}, context)


def check(u):
    proc = 0
    proc = proc+1 if u.first_name else proc
    proc = proc+1 if u.last_name else proc
    proc = proc+1 if u.avatar else proc
    proc = proc+1 if u.telephone else proc
    proc = proc+1 if u.job else proc
    proc = proc+1 if u.date_of_birth else proc
    proc = proc*100/6
    return proc