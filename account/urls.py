from django.conf.urls import patterns, include, url
from account.views import *
 
urlpatterns = patterns('',
    url(r'^$', 'django.contrib.auth.views.login'),
    url(r'^register/$', register, name='register'),
    #url(r'^login/$', SignIn.as_view(), name='sign'),
    url(r'^login/$', user_login, name='login'),
    url(r'^logout/$', user_logout, name='logout'),
    url(r'^profile/$', profile, name='profile'),
)