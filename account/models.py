from django.db import models

from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from sorl.thumbnail.shortcuts import get_thumbnail


class UserManager(BaseUserManager):  # admin model
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError('Enter email address')

        user = self.model(email=UserManager.normalize_email(email), **kwargs)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.create_user(email, password=password, **kwargs)
        user.is_superuser = True
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    username = models.CharField(max_length=255, unique=True)
    avatar = models.ImageField(upload_to='avatars', blank=True, null=True)
    first_name = models.CharField(verbose_name='Name',  max_length=255, blank=True)
    last_name = models.CharField(verbose_name='Surname',  max_length=255, blank=True)
    date_of_birth = models.DateField(verbose_name='Birthday',  blank=True, null=True)
    telephone = models.CharField(verbose_name='Telephone',  max_length=20, blank=True)
    job = models.CharField(verbose_name='Job',  max_length=40, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    def get_thumbnail_html(self):
        img = self.avatar
        if img:
            img_resize_url = unicode(get_thumbnail(img, '100x100').url)
            html = '<a class="image-picker" href="%s"><img src="%s" alt="%s"/></a>'
            return html % (self.avatar.url, img_resize_url, self.username)
    get_thumbnail_html.short_description = 'Avatar'
    get_thumbnail_html.allow_tags = True

    def delete(self, *args, **kwargs):
        print self.avatar.path
        self.avatar.storage.delete(self.avatar.path)

    # after this comment - fetches for the future
    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name,)

    def get_short_name(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin